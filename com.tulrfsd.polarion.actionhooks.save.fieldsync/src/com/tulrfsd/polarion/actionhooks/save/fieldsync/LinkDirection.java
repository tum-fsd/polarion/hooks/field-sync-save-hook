package com.tulrfsd.polarion.actionhooks.save.fieldsync;

public enum LinkDirection {
  DIRECT,
  BACK,
}
