package com.tulrfsd.polarion.actionhooks.save.fieldsync;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Properties;
import com.adva.polarion.actionhook.IActionHook;
import com.polarion.alm.tracker.model.ILinkedWorkItemStruct;
import com.polarion.alm.tracker.model.IPlan;
import com.polarion.alm.tracker.model.ITestRun;
import com.polarion.alm.tracker.model.IWorkItem;
import com.polarion.core.util.logging.Logger;

public class FieldSync implements IActionHook {

  public static final String HOOK_ID = "FieldSync";
  public static final String HOOK_DESCRIPTION =
      "Syncs the specified fields to linked work items from the source to the target work items";
  private static final String HOOK_VERSION = "1.1.2";
  
  private static Logger logger = Logger.getLogger(FieldSync.class.getName());

  private HashMap<String, String> hookProperties = new HashMap<>();
  private HashSet<String> workItemBlacklist = new HashSet<>();
  private HashMap<String, HashMap<String, ArrayList<SyncConfig>>> syncConfigMap = new HashMap<>();
  
  @Override
  public ACTION_TYPE getAction() {
    return ACTION_TYPE.SAVE;
  }

  @Override
  public String getName() {
    return HOOK_ID;
  }

  @Override
  public String getDescription() {
    return HOOK_DESCRIPTION;
  }

  @Override
  public String getVersion() {
    return HOOK_VERSION;
  }

  @Override
  public boolean isEnabled() {
 // deprecated in interface, controlled by properties file
    return true;
  }

  @Override
  public void setEnabled(boolean arg0) {
 // deprecated in interface
  }

  @Override
  public void init() {
    // no hook init required
  }

  @Override
  public void initProperties(Properties properties) {

    this.workItemBlacklist.clear();
    this.syncConfigMap.clear();
    this.hookProperties.clear();
    
    if (properties == null) {
      return;
    }

  // read all properties from the global file belonging to this hook
    for (String propertyKey : properties.stringPropertyNames()) {
      if (propertyKey.startsWith(this.getName() + ".") && !properties.getProperty(propertyKey).isEmpty()) {
        this.hookProperties.put(propertyKey.replace(this.getName() + ".", ""), properties.getProperty(propertyKey));
      }
    }
  }

  @Override
  public String processAction(IWorkItem workItem) {
        
    // try/catch to make sure that Polarion can still be used even if something goes wrong with this hook
    try {
      String projectAndWorkItemItemTypeId = workItem.getProjectId() + "." + workItem.getType().getId();
      
      // check if combination of project and work item type is already on blacklist
      if (workItemBlacklist.contains(projectAndWorkItemItemTypeId)){
        return null;
      }
      
      // check if configuration is already loaded for project/workItem type combination
      if (!syncConfigMap.containsKey(projectAndWorkItemItemTypeId)) {
        // configuration will either be loaded or project/workItem type combination put on blacklist
        HashMap<String, ArrayList<SyncConfig>> fieldSyncConfigMap = new HashMap<>();
        // load specific and generic definitions (all will be applied!)
        putSyncConfigInMap(fieldSyncConfigMap, projectAndWorkItemItemTypeId, workItem.getProjectId(), workItem.getType().getId());
        putSyncConfigInMap(fieldSyncConfigMap, workItem.getProjectId() + ".*", workItem.getProjectId(), workItem.getType().getId());
        putSyncConfigInMap(fieldSyncConfigMap, "*." + workItem.getType().getId(), workItem.getProjectId(), workItem.getType().getId());
        putSyncConfigInMap(fieldSyncConfigMap, "*.*", workItem.getProjectId(), workItem.getType().getId());

        if (fieldSyncConfigMap.isEmpty()) {
          workItemBlacklist.add(projectAndWorkItemItemTypeId);
        } else {
          syncConfigMap.put(projectAndWorkItemItemTypeId, fieldSyncConfigMap);
        }
      }
      
      if (syncConfigMap.containsKey(projectAndWorkItemItemTypeId)) {
        processWorkItem(workItem, syncConfigMap.get(projectAndWorkItemItemTypeId));
      }
      
    } catch (Exception e) {
      // write exception in log
      logger.error("While processing work item " + workItem.getId() + ": " + e);
    }

    return null;
  }


  private void processWorkItem(IWorkItem workItem, HashMap<String, ArrayList<SyncConfig>> fieldSyncConfigMap) {

    // check necessary syncs for each work item field specified in the configuration
    for (ArrayList<SyncConfig> syncConfigList : fieldSyncConfigMap.values()) {
      
      for (SyncConfig syncConfig : syncConfigList) {
        
     // get linked work items according to the link direction specified in the config
        HashSet<ILinkedWorkItemStruct> linkedWiStructSet = new HashSet<>();
        if (syncConfig.getLinkDirection() == LinkDirection.DIRECT) {
          linkedWiStructSet.addAll(workItem.getLinkedWorkItemsStructsDirect());
        } else if (syncConfig.getLinkDirection() == LinkDirection.BACK) {
          linkedWiStructSet.addAll(workItem.getLinkedWorkItemsStructsBack());
        }

        String sourceFieldTypeName = HookUtils.getFieldTypeName(workItem, syncConfig.getSourceFieldId());

        // check all linked work items for those requiring a sync
        for (ILinkedWorkItemStruct linkedWiStruct : linkedWiStructSet) {
          IWorkItem targetWorkItem = linkedWiStruct.getLinkedItem();
          if (targetWorkItem.isUnresolvable()) {
            continue;
          }
          String targetFieldTypeName = HookUtils.getFieldTypeName(targetWorkItem, syncConfig.getTargetFieldId());
          String linkRoleId = linkedWiStruct.getLinkRole().getId();
          if (linkRoleId.equals(syncConfig.getLinkRoleId()) // link role needs to match
              && targetWorkItem.getProjectId().equals(syncConfig.getTargetProjectId()) // target project needs to match
              && targetWorkItem.getType().getId().equals(syncConfig.getTargetWorkItemTypeId()) // target work item type needs to match
              && !sourceFieldTypeName.isEmpty() && !targetFieldTypeName.isEmpty() // source and target work item fields need to be defined and supported
              && sourceFieldTypeName.equals(targetFieldTypeName)) /* work item field types need to match */ {
            syncWorkItem(workItem, targetWorkItem, syncConfig);
          }
        }
      }
    }
  }
  
  @Override
  public String processAction(IPlan plan) {
    // hook not configured for plans
    return null;
  }

  @Override
  public String processAction(ITestRun testRun) {
    // hook not configured for testRuns
    return null;
  }

  private void syncWorkItem(IWorkItem sourceWorkItem, IWorkItem targetWorkItem, SyncConfig syncConfig) {

    if (syncConfig.getSyncMode().equals(SyncMode.SINGLE)) {
      targetWorkItem.setValue(syncConfig.getTargetFieldId(), sourceWorkItem.getValue(syncConfig.getSourceFieldId()));
      targetWorkItem.save();

    } else if (syncConfig.getSyncMode().equals(SyncMode.MIN) || syncConfig.getSyncMode().equals(SyncMode.MAX)) {
      // back and direct linking is now switched because it is defined from the source perspective
      // get all linked work items based on the configuration
      HashSet<ILinkedWorkItemStruct> linkedWiStructSet = new HashSet<>();
      if (syncConfig.getLinkDirection() == LinkDirection.BACK) {
        linkedWiStructSet.addAll(targetWorkItem.getLinkedWorkItemsStructsDirect());
      } else if (syncConfig.getLinkDirection() == LinkDirection.DIRECT) {
        linkedWiStructSet.addAll(targetWorkItem.getLinkedWorkItemsStructsBack());
      }

      ArrayList<Object> fieldList = new ArrayList<>();
      // Source work item might not be visible for the target work item in case it is newly created, or the link is newly created
      // Anyway, the field value is relevant and can thus directly be added to the list. Duplicates in the list don't cause any trouble.
      Object fieldValue = sourceWorkItem.getValue(syncConfig.getSourceFieldId());
      if(fieldValue != null) {
        fieldList.add(fieldValue);
      }
      
      for (ILinkedWorkItemStruct linkedWiStruct : linkedWiStructSet) {
        IWorkItem linkedWorkItem = linkedWiStruct.getLinkedItem();
        if (linkedWorkItem.isUnresolvable()) {
          continue;
        }
        // since the just edited work item is not yet saved, it cannot be read from the linked work
        // items of the target work item (there, the change is not yet reflected), so it has to be
        // taken from the method input parameters
        if (linkedWorkItem.getUri().toString().equals(sourceWorkItem.getUri().toString())) {
          linkedWorkItem = sourceWorkItem;
        }

        // gather fields for syncing
        String linkRoleId = linkedWiStruct.getLinkRole().getId();
        if (linkRoleId.equals(syncConfig.getLinkRoleId()) // link role needs to match
            && linkedWorkItem.getProjectId().equals(syncConfig.getSourceProjectId()) // source project needs to match
            && linkedWorkItem.getType().getId().equals(syncConfig.getSourceWorkItemTypeId())) /* source work item type needs to match */ {
          // add field to list for later comparison
          fieldValue = linkedWorkItem.getValue(syncConfig.getSourceFieldId());
          if(fieldValue != null) {
            fieldList.add(fieldValue);
          }
        }
      }
      
      Object oldFieldValue = targetWorkItem.getValue(syncConfig.getTargetFieldId());
      Object newFieldValue = HookUtils.getFieldValueFromList(fieldList, syncConfig.getSyncMode());
      if (!(oldFieldValue == null && newFieldValue == null) &&
          ((oldFieldValue == null && newFieldValue != null) || (oldFieldValue != null && newFieldValue == null) || !newFieldValue.equals(oldFieldValue))) {
        targetWorkItem.setValue(syncConfig.getTargetFieldId(), newFieldValue);
        targetWorkItem.save();
      }
    }
  }


  private void putSyncConfigInMap(HashMap<String, ArrayList<SyncConfig>> fieldSyncConfigMap, String propertySearchString, String projectId, String workItemTypeId) {

    for (Map.Entry<String, String> entry : hookProperties.entrySet()) {
      
      String propertyKey = entry.getKey();
      
      if (propertyKey.startsWith(propertySearchString + ".")) {
        String propertyValue = entry.getValue().replaceAll("\\s", "");
        String[] values = propertyValue.split(","); // split comma separated entries in property value
        String[] keys = propertyKey.split("\\."); // split dot separated entries in property key
        
        // only process data if properties file matches syntax
        if (values.length == 6 && (keys.length == 3 || keys.length == 4)) {
          SyncConfig config = new SyncConfig(projectId, workItemTypeId, keys[2], values[0], values[1],
              values[2], values[3], values[4], values[5]);
          
          if (checkBidirectionalSyncing(config, fieldSyncConfigMap)) {
            // ignore configuration when it would lead to bidirectional syncing and thus endless loops
            logger.warn("The property key " + propertyKey + " would have lead to cyclic syncing and is thus ignored.");
            continue;
          }
          
          if(fieldSyncConfigMap.containsKey(keys[2])) {
            fieldSyncConfigMap.get(keys[2]).add(config); // adds entry in already existing array list inside the HashMap
          } else {
            ArrayList<SyncConfig> fieldSyncConfigList = new ArrayList<>();
            fieldSyncConfigList.add(config);
            fieldSyncConfigMap.put(keys[2], fieldSyncConfigList);
          }
          
        } else {
          if (keys.length < 3 || keys.length > 4) {
            logger.warn("The property key " + propertyKey + " does not have the format <Hook Name>.<Project ID>.<Work Item Type ID>.<Field ID>.<Optional string for uniqueness>");
          }
          if (values.length != 6) {
            logger.warn("The property value for the key " + propertyKey + " must be a comma separated list with the 6 entries ProjectID,WorkItemTypeID,FieldID,LinkRoleID,LinkDirection,SyncMode. Check the hook documentation for details.");
          }
        }
      }
    }
  }

  private boolean checkBidirectionalSyncing(SyncConfig config, HashMap<String, ArrayList<SyncConfig>> fieldSyncConfigMap) {
    
    // check all field configs for the current work item type
    for (ArrayList<SyncConfig> configList : fieldSyncConfigMap.values()) {
      for (SyncConfig existingConfig : configList) {
        if (config.getSourceProjectId().equals(existingConfig.getTargetProjectId()) && config.getSourceWorkItemTypeId().equals(existingConfig.getTargetWorkItemTypeId()) &&
            config.getTargetProjectId().equals(existingConfig.getSourceProjectId()) && config.getTargetWorkItemTypeId().equals(existingConfig.getSourceWorkItemTypeId()) &&
           !config.getLinkDirection().equals(existingConfig.getLinkDirection())) {
          return true;
        }
      }
    }
    
    // check all configs for already processed work item types
    for (HashMap<String, ArrayList<SyncConfig>> existingFieldSyncConfigMap : this.syncConfigMap.values()) {
      for (ArrayList<SyncConfig> configList : existingFieldSyncConfigMap.values()) {
        for (SyncConfig existingConfig : configList) {
          if (config.getSourceProjectId().equals(existingConfig.getTargetProjectId()) && config.getSourceWorkItemTypeId().equals(existingConfig.getTargetWorkItemTypeId()) &&
              config.getTargetProjectId().equals(existingConfig.getSourceProjectId()) && config.getTargetWorkItemTypeId().equals(existingConfig.getSourceWorkItemTypeId()) &&
             !config.getLinkDirection().equals(existingConfig.getLinkDirection())) {
            return true;
          }
        }
      }
    }
    return false;
  }
}
