package com.tulrfsd.polarion.actionhooks.save.fieldsync;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import com.polarion.alm.tracker.model.IWorkItem;
import com.polarion.core.util.types.Text;
import com.polarion.core.util.types.duration.DurationTime;
import com.polarion.subterra.base.data.model.IPrimitiveType;
import com.polarion.subterra.base.data.model.IType;

public interface HookUtils {
  
  public static boolean isFieldDefined(IWorkItem workItem, String fieldId) {
    return workItem.getPrototype().getKeyNames().contains(fieldId) || workItem.getCustomFieldsList().contains(fieldId);
  }
  
  public static String getFieldTypeName(IWorkItem workItem, String fieldId) {
    
    if (!isFieldDefined(workItem, fieldId)) {
      return "";
    }
    
    IType fieldType = workItem.getFieldType(fieldId);
    
    // currently, only primitive field types are supported
    if (!(fieldType instanceof IPrimitiveType)) {
      return "";
    }
    String fieldTypeName = ((IPrimitiveType) fieldType).getTypeName();
    switch (fieldTypeName) {
      // list of supported primitive field types
      case "com.polarion.core.util.types.Text":
      case "com.polarion.core.util.types.duration.DurationTime":
      case "com.polarion.core.util.types.DateOnly":
      case "java.util.Date":
      case "java.lang.Integer":
      case "java.lang.Float":
      case "java.lang.Boolean":
      case "com.polarion.core.util.types.Currency":
      case "com.polarion.core.util.types.TimeOnly":
      case "java.lang.String":
        return fieldTypeName;
      default:
        return "";
    }
  }

  @SuppressWarnings({"rawtypes", "unchecked"})
  public static Object getFieldValueFromList(List<Object> fieldList, SyncMode syncMode) {

    if (fieldList.isEmpty()) {
      return null;
    } else if (fieldList.size() == 1) {
      return fieldList.get(0);
    }
      
    Object field = fieldList.get(0);

    switch (field.getClass().getName()) {
      // DurationTime does not implement Comparable and thus needs special treatment
      case "com.polarion.core.util.types.duration.DurationTime":
        for (Object listElement : fieldList.subList(1, fieldList.size())) {
          int compareOrder = 0;
          compareOrder = Long.compare(((DurationTime) field).getMillis(), ((DurationTime) listElement).getMillis());
          field = getCompareResult(field, listElement, compareOrder, syncMode);
        }
        return field;
        
      // Text does not implement Comparable and thus needs special treatment
      case "com.polarion.core.util.types.Text":
        for (Object listElement : fieldList.subList(1, fieldList.size())) {
          int compareOrder = 0;
          compareOrder = ((Text) field).toString().compareTo(((Text) listElement).toString());
          field = getCompareResult(field, listElement, compareOrder, syncMode);
        }
        return field;
        
      case "com.polarion.core.util.types.DateOnly":
      case "java.util.Date":
      case "java.lang.Integer":
      case "java.lang.Float":
      case "java.lang.Boolean":
      case "com.polarion.core.util.types.Currency":
      case "com.polarion.core.util.types.TimeOnly":
      case "java.lang.String":
        if (syncMode == SyncMode.MIN) {
          return Collections.min(((Collection) fieldList));
        } else if (syncMode == SyncMode.MAX) {
          return Collections.max(((Collection) fieldList));
        } else {
          return null;
        }
      default:
        return null;
    }
  }
  
  private static Object getCompareResult(Object first, Object second, int compareOrder, SyncMode syncMode) {
    if ((compareOrder < 0 && syncMode == SyncMode.MAX) || (compareOrder > 0 && syncMode == SyncMode.MIN)) {
      return second;
    }
    return first;
  }
}